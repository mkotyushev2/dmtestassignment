# Intro

This repo contains e-mail filter implementation in Python and minimal example with running local `postfix` send- and receive- server to see how it works.

The project structure is as follows:

- `docker-compose.yml` including a `milter` service, a service for `postfix` server which operate in the same network
- `.env` containing network settings
- `Dockerfile_*` files for both services
- `params.yml` filtering parameters (see below)
- `milter/`: `milter` service Python entry point
- `files/`: `postfix` service setup script and entrypoint
-  `example_message_bodies/`: examples of the e-mail message bodies (see **Some examples** section below) 
- `.devcontainer/`: .vscode development folder

### Progress
The progress in the test assignment is as follows:

1. Analyzer is implemented, but there is no persistent storage (e.g. database) to store read the values to match from and to store the incoming values.
2. Parameters are read from the provided `params.yml` file and could be configured.
3. The app is dockerized and `docker-compose.yml` is provided, no CI/CD implemented.


# Parameters

- `format_regex`: is a regex to match e-mail body
- `from_adresses_of_interest`: a list of e-mail addresses in which sender should be present pass the filter
- `to_adresses_of_interest`: a list of e-mail addresses in which receiver should be present pass the filter
- `values_to_match`: dictionary where key is a regex group name and value is corresponding value. If the message passes format check and addresses check, it is parsed with `format_regex` and values are compared with `values_to_match`.

# Requirements

- Ubuntu

- Docker

- swaks (`sudo apt install swaks`) for sending e-mails

- non-occupied `172.23.0.0/16` subnetwork (if it is occupied, change corresponding values in `.env` to some free local subnetwork)

# Starting example

1. Start simple local postfix server and example milter services

```
docker compose up -d
```

2. In the second terminal, connect to milter container's logs to see output of debug example milter

```
docker logs -f -t milter
```

3. In the third terminal, send an example e-mail through postfix server

```
swaks --to user@test.local --server 172.23.0.3
```

After that, in the second terminal with logs some debug prints should appear.

# Some examples

Matching with body from file

```
swaks --to user3@test.local --from user1@test.local --server 172.23.0.3 --body example_message_bodies/example_matching_body.txt
```

Non-matching by to address

```
swaks --to user@test.local --from user1@test.local --server 172.23.0.3 --body example_message_bodies/example_matching_body.txt
```

Non-matching by from address

```
swaks --to user1@test.local --from user@test.local --server 172.23.0.3 --body example_message_bodies/example_matching_body.txt
```

Non-matching by format

```
swaks --to user1@test.local --from user@test.local --server 172.23.0.3 --body example_message_bodies/example_non_matching_body_by_name.txt
```

Non-matching by name

```
swaks --to user1@test.local --from user@test.local --server 172.23.0.3 --body example_message_bodies/example_non_matching_body_by_name.txt
```

