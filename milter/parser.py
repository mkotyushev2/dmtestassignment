
import attrs
import logging
import re
import purepythonmilter
from asyncio import Lock
from collections import defaultdict
from purepythonmilter.api.models import connection_id_context
from typing import Set, Dict


logger = logging.getLogger(__file__)


@attrs.define()
class Message:
    from_ok: bool = attrs.field(default=False, init=False)
    to_ok: bool = attrs.field(default=False, init=False)
    body: None | str = attrs.field(default=None, init=False)


class Parser:
    def __init__(
        self, 
        from_adresses_of_interest: Set[str], 
        to_adresses_of_interest: Set[str],
        format_regex: str,
        values_to_match: Dict[str, str]
    ):
        self.from_adresses_of_interest: Set[str] = from_adresses_of_interest
        self.to_adresses_of_interest: Set[str] = to_adresses_of_interest
        self.messages: Dict[str] = defaultdict(Message)
        self.lock: Lock = Lock()
        self.format_regex: re.Pattern = re.compile(format_regex)
        self.values_to_match: Dict[str, str] = values_to_match
        # TODO: add persistent storage
        self.storage = dict()
    
    async def on_mail_from(self, cmd: purepythonmilter.MailFrom) -> None:
        async with self.lock:
            message_id = connection_id_context.get()
            logger.info(
                f"{message_id} On MAIL FROM: address={cmd.address}, esmtp_args={cmd.esmtp_args}, "
                f"macros={cmd.macros}"
            )

            if cmd.address in self.from_adresses_of_interest:
                self.messages[message_id].from_ok = True

    async def on_rcpt_to(self, cmd: purepythonmilter.RcptTo) -> None:
        async with self.lock:
            message_id = connection_id_context.get()
            logger.info(
                f"{message_id} On RCPT TO: address={cmd.address}, esmtp_args={cmd.esmtp_args}, "
                f"macros={cmd.macros}"
            )

            if cmd.address in self.to_adresses_of_interest:
                self.messages[message_id].to_ok = True

    async def on_body_chunk(self, cmd: purepythonmilter.BodyChunk) -> None:
        async with self.lock:
            message_id = connection_id_context.get()
            logger.info(f"{message_id} On body chunk: length={len(cmd.data_raw)}, macros={cmd.macros}")

            if self.messages[message_id].body is None:
                self.messages[message_id].body = cmd.data_raw
            else:
                self.messages[message_id].body = self.messages[message_id].body + cmd.data_raw

    async def on_end_of_message(self, cmd: purepythonmilter.EndOfMessage) -> None:
        async with self.lock:
            message_id = connection_id_context.get()
            logger.info(f"{message_id} On end of message: macros={cmd.macros}")
            
            if message_id not in self.messages:
                logger.info(
                    f'Not matched because of from / to fields were not received. '
                )
                return 

            if (
                not self.messages[message_id].from_ok or 
                not self.messages[message_id].to_ok
            ):
                logger.info(
                    f'Not matched because of from / to fields are '
                    f'not from the provided lists. Message: {self.messages[message_id]}'
                )
                return
            
            logger.info(
                f"Full received message {message_id} "
                f"with body {self.messages[message_id].body}"
            )

            body = self.messages[message_id].body.decode()
            del self.messages[message_id]

        matches = list(self.format_regex.finditer(body))
        if len(matches) == 1:
            fielddict = matches[0].groupdict()
            # Exactly matches
            logger.info(f'Matched: {fielddict}')

            for key, value in self.values_to_match.items():
                if key not in fielddict or fielddict[key] != value:
                    logger.info(f'Not matched by {key} (expected {value}, got {fielddict[key]}).')
                    return
            
            self.storage[message_id] = fielddict
            logger.info(f'Current values: {self.storage}')
        else:
            # Do not match or many matches, skipping
            logger.info(f'Not matched format.')

