# SPDX-FileCopyrightText: 2023 Gert van Dijk <github@gertvandijk.nl>
#
# SPDX-License-Identifier: Apache-2.0

from __future__ import annotations

import logging
import click
import purepythonmilter
import yaml

from pathlib import Path
from purepythonmilter import PurePythonMilter
from typing import Set

from parser import Parser



@click.command(
    context_settings={
        "show_default": True,
        "max_content_width": 200,
        "auto_envvar_prefix": "PUREPYTHONMILTER",
    }
)
@click.option(
    "--bind-host", default=purepythonmilter.DEFAULT_LISTENING_TCP_IP, show_envvar=True
)
@click.option(
    "--bind-port", default=purepythonmilter.DEFAULT_LISTENING_TCP_PORT, show_envvar=True
)
@click.option(
    "--params-filepath", 
    type=Path,
    default=Path('params.yml'), 
    show_envvar=True
)
@click.option(
    "--log-level",
    type=click.Choice(["DEBUG", "INFO", "WARNING", "ERROR"], case_sensitive=False),
    default="INFO",
    show_envvar=True,
)
@click.version_option(package_name="purepythonmilter", message="%(version)s")
def main(*, bind_host: str, bind_port: int, params_filepath: Path, log_level: str) -> None:
    """
    This Milter app only logs all events for debugging purposes.
    """

    with params_filepath.open('r') as f:
        params = yaml.safe_load(f)

    parser = Parser(
        from_adresses_of_interest=params['from_adresses_of_interest'],
        to_adresses_of_interest=params['to_adresses_of_interest'],
        format_regex=params['format_regex'],
        values_to_match=params['values_to_match']
    )
    debug_log_all_milter = PurePythonMilter(
        name="debug_log_all",
        on_rcpt_to_include_rejected=True,
        restrict_symbols=None,
        hook_on_mail_from=parser.on_mail_from,
        hook_on_rcpt_to=parser.on_rcpt_to,
        hook_on_body_chunk=parser.on_body_chunk,
        hook_on_end_of_message=parser.on_end_of_message,
    )

    logging.basicConfig(level=getattr(logging, log_level))
    debug_log_all_milter.run_server(host=bind_host, port=bind_port)


if __name__ == "__main__":
    main()
